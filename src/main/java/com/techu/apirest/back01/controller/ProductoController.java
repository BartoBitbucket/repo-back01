package com.techu.apirest.back01.controller;

import com.techu.apirest.back01.model.ProductoModel;
import com.techu.apirest.back01.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
        if(productoService.findById(productoToDelete.getId()).isPresent()){
            productoService.delete(productoToDelete);
            return true;
        }
        return false;
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductos(@PathVariable String id) {
        if (productoService.findById(id).isPresent()) {
            productoService.deleteById(id);
            return true;
        }
        return false;
    }

    @PatchMapping("/productos/{precio}")
    public boolean patchProductos(@RequestBody ProductoModel productoToPatch, @PathVariable Double precio){
        if(productoService.findById(productoToPatch.getId()).isPresent()){
            productoToPatch.setPrecio(precio);
            productoService.save(productoToPatch);
            return true;
        }
        else{
            return false;
        }
    }

}
