package com.techu.apirest.back01.repository;

import com.techu.apirest.back01.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository public interface ProductoRepository extends MongoRepository<ProductoModel, String> { }
