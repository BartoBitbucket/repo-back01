package com.techu.apirest.back01.service;

import com.techu.apirest.back01.model.ProductoModel;
import com.techu.apirest.back01.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String  id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    //Delete
    public boolean deleteById(String id){
        try{
            productoRepository.deleteById(id);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
